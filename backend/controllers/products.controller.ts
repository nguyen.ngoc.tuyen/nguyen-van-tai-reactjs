import fileUpload = require('express-fileupload');
import { Request, Response } from 'express';
import Products from '../models/products.model';

type UploadedFile = fileUpload.UploadedFile;

export const getProducts = async (req: Request, res: Response) => {
  const pageOptions = {
    page: parseInt(req.query.page as any, 10) || 1,
    limit: parseInt(req.query.limit as any, 10) || 8,
  };
  if (req.query.page && req.query.limit) {
    const products = await Products.find()
      .skip(pageOptions.page * pageOptions.limit - pageOptions.limit)
      .limit(pageOptions.limit);
    const countProducts = await Products.estimatedDocumentCount();
    return res.json({
      products,
      countProducts,
    });
  }
  if (req.query.name) {
    const products = await Products.find({
      name: { $regex: req.query.name, $options: '$i' },
    })
      .skip(pageOptions.page * pageOptions.limit - pageOptions.limit)
      .limit(pageOptions.limit);
    const countProducts = await Products.find({
      category: req.query.category,
    }).count();
    return res.json({ products, countProducts });
  }
  if (req.query.category && !req.query.type) {
    const products = await Products.find({ category: req.query.category })
      .skip(pageOptions.page * pageOptions.limit - pageOptions.limit)
      .limit(pageOptions.limit);
    const countProducts = await Products.find({
      category: req.query.category,
    }).count();
    return res.json({ products, countProducts });
  }
  if (req.query.category && req.query.type) {
    const products = await Products.find({
      category: req.query.category,
      type: req.query.type,
    })
      .skip(pageOptions.page * pageOptions.limit - pageOptions.limit)
      .limit(pageOptions.limit);
    const countProducts = products.length;
    return res.json({ products, countProducts });
  }
};

export const getCategory = (req: Request, res: Response) => {
  return res.json([
    {
      category: 'Quần',
      slug: 'Quan',
      type: [
        { title: 'Quần đùi', slug: 'Quan-dui' },
        { title: 'Quần dài', slug: 'Quan-dai' },
      ],
    },
    {
      category: 'Áo',
      slug: 'Ao',
      type: [
        { title: 'Áo dài tay', slug: 'Ao-dai-tay' },
        { title: 'Áo ngắn tay', slug: 'Ao-ngan-tay' },
      ],
    },
  ]);
};

export const createProduct = async (req: Request, res: Response) => {
  const product = await Products.create(req.body);
  res.json(product);
};

export const removeProduct = async (req: Request, res: Response) => {
  await Products.findOneAndDelete(
    { _id: req.params.id },
    (err) => err && console.log(err)
  );
  res.json({ message: 'Deleted' });
};

export const updateProduct = async (req: Request, res: Response) => {
  const product = await Products.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    (err) => err && console.log(err)
  );
  res.json(product);
};

export const getProductById = async (req: Request, res: Response) => {
  const product = await Products.findOne({ _id: req.params.id });
  return res.json(product);
};

export const uploadImageProduct = async (req: Request, res: Response) => {
  if (!req.files) {
    return res.status(400).send('No files were uploaded.');
  }
  const images = req.files.images as UploadedFile;
  images.mv(`./uploads/${images.name}`);
  res.json({ fileName: `${process.env.HOST}${images.name}` });
};
