import { Schema, model } from 'mongoose';
const ProductSchema = new Schema({
  name: { type: String, required: true, unique: true },
  price: { type: String, required: true },
  type: { type: String, required: true },
  category: { type: String, required: true },
  description: String,
  images: [String],
});

const Products = model('Products', ProductSchema, 'products');
export default Products;
