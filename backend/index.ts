import express from 'express';
import dotenv from 'dotenv';
dotenv.config();
import morgan from 'morgan';
import bodyParser from 'body-parser';
import cors from 'cors';
import mongoose from 'mongoose';
import fileUpload from 'express-fileupload';
import productsRoute from './routes/products.route';
import { MONGO_URL } from './constant';
const app = express();
mongoose.connect(MONGO_URL || process.env.MONGO_URL, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
});

app.use(morgan('dev'));
app.use(cors());
// Parse the request
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.use(express.static('uploads'));
app.use(
  fileUpload({
    createParentPath: true,
  })
);
app.use(express.urlencoded({ extended: true }));
app.use('/api/products', productsRoute);

app.listen(process.env.PORT || 3000, () => console.log('Start server'));
