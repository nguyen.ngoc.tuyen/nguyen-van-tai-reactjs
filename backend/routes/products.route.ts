import express from 'express';
import {
  getProducts,
  createProduct,
  removeProduct,
  updateProduct,
  getProductById,
  uploadImageProduct,
  getCategory,
} from '../controllers/products.controller';

const router = express.Router();

router.get('/', getProducts);
router.get('/category', getCategory);
router.post('/create', createProduct);
router.get('/:id', getProductById);
router.post('/upload', uploadImageProduct);
router.delete('/:id', removeProduct);
router.patch('/:id', updateProduct);
export default router;
