import axios from 'axios';
import { Product } from '../store/product/types';
const getAllProduct = ({ page, limit }: any) =>
  axios.get(
    `${process.env.REACT_APP_URL_API}/products?page=${page}&limit=${limit}`
  );
const getAllCategory = () =>
  axios.get(`${process.env.REACT_APP_URL_API}/products/category`);
const getProductByName = (name: any) =>
  axios.get(`${process.env.REACT_APP_URL_API}/products?name=${name}`);
const getProductByCategory = (category: any) =>
  axios.get(`${process.env.REACT_APP_URL_API}/products?category=${category}`);
const getProductByType = (category: any, type: any) =>
  axios.get(
    `${process.env.REACT_APP_URL_API}/products?category=${category}&type=${type}`
  );
const getProductById = (_id: any) =>
  axios.get(`${process.env.REACT_APP_URL_API}/products/${_id}`);
const createProduct = (product: Product) =>
  axios.post(`${process.env.REACT_APP_URL_API}/products/create`, product);
const deleteProduct = (id: string) =>
  axios.delete(`${process.env.REACT_APP_URL_API}/products/${id}`);
const uploadImages = (formData: any) =>
  axios.post(`${process.env.REACT_APP_URL_API}/products/upload`, formData);

export {
  getAllProduct,
  createProduct,
  uploadImages,
  deleteProduct,
  getProductByName,
  getProductById,
  getProductByCategory,
  getProductByType,
  getAllCategory,
};
