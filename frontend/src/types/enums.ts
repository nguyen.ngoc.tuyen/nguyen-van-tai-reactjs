export enum Routes {
  HOME = '/',
  MANAGER_PRODUCTS = '/manager-products',
  UPDATE_PRODUCT = '/manager-products/update/:id',
  DETAIL_PRODUCT = '/:id',
}
