type StringOrNumber = string | number | undefined;
export const formatMoney = (value: StringOrNumber): string =>
  `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
