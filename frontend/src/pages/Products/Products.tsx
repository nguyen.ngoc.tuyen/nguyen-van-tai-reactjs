import React, { Dispatch, FC, useEffect, useState } from 'react';
import { useSelector, shallowEqual, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import {
  getCategory,
  getProducts,
  getProductsByCategory,
  getProductsByName,
  getProductsByType,
} from '../../store/product/actions';
import strings from '../../strings';
import {
  Layout,
  Breadcrumb,
  PageHeader,
  Select,
  Input,
  Row,
  Pagination,
} from 'antd';
import MenuSideBar from '../../components/MenuSideBar';
import ProductCard from '../../components/ProductCard';
import { Product } from '../../store/product/types';

const { Search } = Input;
const { Option } = Select;
const { Content, Sider } = Layout;

const Products: FC = () => {
  const [panigation, setPanigation] = useState({
    page: 1,
    limit: 8,
  });
  const [category, setCategory] = useState('');
  const [listType, setlistType] = useState([]);
  const dispatch: Dispatch<any> = useDispatch();
  const products = useSelector(
    (state: RootState) => state.product.products,
    shallowEqual
  );
  const countProduct = useSelector(
    (state: RootState) => state.product.totalPages,
    shallowEqual
  );
  const listCategory = useSelector(
    (state: RootState) => state.product.category,
    shallowEqual
  );
  useEffect(() => dispatch(getProducts(panigation)), [dispatch, panigation]);
  useEffect(() => dispatch(getCategory()), [dispatch]);

  const handleChangeCategory = (value: any) => {
    const type: any = listCategory.find((item: any) => item.slug === value);
    setlistType(type.type);
    setCategory(value);
    dispatch(getProductsByCategory(value));
  };
  const handleChangeType = (value: any) => {
    dispatch(getProductsByType(category, value));
  };

  const onChangePage = (page: any, pageSize: any) => {
    setPanigation({ page: page, limit: pageSize });
  };

  const handleSearchName = (name: string) => {
    dispatch(getProductsByName(name));
  };

  return (
    <Layout>
      <Sider
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
        }}
      >
        <div
          style={{
            height: '32px',
            background: 'rgba(255, 255, 255, 0.2)',
            margin: '16px',
          }}
        />
        <MenuSideBar />
      </Sider>
      <Layout
        className="site-layout"
        style={{
          marginLeft: 200,
          minHeight: '100vh',
          height: '100%',
          overflow: 'hidden',
        }}
      >
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
          <Breadcrumb separator=">">
            <Breadcrumb.Item>{strings.management}</Breadcrumb.Item>
            <Breadcrumb.Item href="">
              {strings.applicationCenter}
            </Breadcrumb.Item>
          </Breadcrumb>
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Title"
            subTitle="This is a subtitle"
            extra={[
              <Select
                defaultValue="Type"
                style={{ width: 120 }}
                key="1"
                onChange={handleChangeType}
              >
                {listType &&
                  listType.map((item: any) => (
                    <Option key={item.slug} value={item.slug}>
                      {item.title}
                    </Option>
                  ))}
              </Select>,
              <Select
                defaultValue="Category"
                key="2"
                style={{ width: 120 }}
                onChange={handleChangeCategory}
              >
                {listCategory &&
                  listCategory.map((item: any) => (
                    <Option key={item.slug} value={item.slug}>
                      {item.category}
                    </Option>
                  ))}
              </Select>,
              <Search
                placeholder="input search text"
                onSearch={handleSearchName}
                key="3"
                enterButton
                style={{ width: 200 }}
              />,
            ]}
            style={{
              backgroundColor: '#f5f5f5',
              padding: '24px',
            }}
          ></PageHeader>
          <Row gutter={[16, 24]} wrap={true}>
            {products &&
              products.map((product: Product) => (
                <ProductCard key={product._id} product={product} />
              ))}
          </Row>
        </Content>
        <Row justify="center" style={{ marginBottom: '20px' }}>
          <Pagination
            pageSize={8}
            defaultCurrent={1}
            total={countProduct}
            onChange={onChangePage}
          />
        </Row>
      </Layout>
    </Layout>
  );
};

export default Products;
