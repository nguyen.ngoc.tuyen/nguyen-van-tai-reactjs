import React, { FC } from 'react';
import { Layout, Breadcrumb, PageHeader, Row, Col } from 'antd';
import ProductForm from '../../components/ProductForm';
import MenuSideBar from '../../components/MenuSideBar';
import TableProducts from '../../components/TableProducts';
import strings from '../../strings';

const { Content, Sider } = Layout;

const ManagerProducts: FC = () => {
  return (
    <Layout>
      <Sider
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
        }}
      >
        <div
          style={{
            height: '32px',
            background: 'rgba(255, 255, 255, 0.2)',
            margin: '16px',
          }}
        />
        <MenuSideBar />
      </Sider>
      <Layout
        className="site-layout"
        style={{
          marginLeft: 200,
          minHeight: '100vh',
          height: '100%',
          overflow: 'hidden',
        }}
      >
        <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
          <Breadcrumb separator=">">
            <Breadcrumb.Item>{strings.management}</Breadcrumb.Item>
            <Breadcrumb.Item href="">{strings.productList}</Breadcrumb.Item>
          </Breadcrumb>
          <PageHeader
            ghost={false}
            onBack={() => window.history.back()}
            title="Product Lists"
            subTitle="This is a subtitle"
            style={{
              backgroundColor: '#f5f5f5',
              padding: '24px',
            }}
          ></PageHeader>
          <Row gutter={[32, 0]} style={{ padding: 16 }}>
            <Col span={16}>
              <TableProducts />
            </Col>
            <Col span={8}>
              <ProductForm />
            </Col>
          </Row>
        </Content>
      </Layout>
    </Layout>
  );
};

export default ManagerProducts;
