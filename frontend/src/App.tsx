import { FC } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Products from './pages/Products';
import ManagerProducts from './pages/ManagerProducts';
import UpdateProduct from './pages/UpdateProduct';
import { Routes } from './types/enums';
import DetailProduct from './pages/DetailProduct';

const App: FC = () => {
  return (
    <Router>
      <Switch>
        <Route path={Routes.HOME} exact component={Products} />
        <Route
          path={Routes.MANAGER_PRODUCTS}
          exact
          component={ManagerProducts}
        />
        <Route path={Routes.UPDATE_PRODUCT} exact component={UpdateProduct} />
        <Route path={Routes.DETAIL_PRODUCT} exact component={DetailProduct} />
        <Route path="*" render={() => <h1>Not found</h1>} />
      </Switch>
    </Router>
  );
};
export default App;
