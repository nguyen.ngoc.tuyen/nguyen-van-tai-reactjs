export default {
  management: 'Products Management',
  products: 'products',
  productList: 'Products List',
  applicationCenter: 'Application Center',
};
