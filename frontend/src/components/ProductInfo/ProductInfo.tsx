import React, { FC } from 'react';
import { Descriptions } from 'antd';
import { RootState } from '../../store';
import { shallowEqual, useSelector } from 'react-redux';
const ProductInfo: FC = () => {
  const currentProduct = useSelector(
    (state: RootState) => state.product.product,
    shallowEqual
  );
  return (
    <Descriptions title={currentProduct.name} size="small" bordered>
      <Descriptions.Item key="id" label="ID" span={3} labelStyle={labelStyle}>
        Zhou Maomao
      </Descriptions.Item>
      <Descriptions.Item
        key="type"
        label="Type"
        span={3}
        labelStyle={labelStyle}
      >
        1810000000
      </Descriptions.Item>
      <Descriptions.Item
        key="category"
        label="Category"
        span={3}
        labelStyle={labelStyle}
      >
        Hangzhou, Zhejiang
      </Descriptions.Item>
      <Descriptions.Item
        key="price"
        label="Price"
        span={3}
        labelStyle={labelStyle}
      >
        empty
      </Descriptions.Item>
      <Descriptions.Item
        key="description"
        label="Description"
        span={3}
        labelStyle={labelStyle}
      >
        No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China
        <br />
        No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China
        <br />
        No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China
        <br />
        No. 18, Wantang Road, Xihu District, Hangzhou, Zhejiang, China No. 18,
        Wantang Road, Xihu District, Hangzhou, Zhejiang, China
      </Descriptions.Item>
    </Descriptions>
  );
};
const labelStyle = {
  fontWeight: 700,
};
export default ProductInfo;
