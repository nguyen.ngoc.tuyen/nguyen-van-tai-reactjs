import React, { FC } from 'react';
import { Menu } from 'antd';
import { Link } from 'react-router-dom';
import { Routes } from '../../types/enums';
const MenuSideBar: FC = () => {
  return (
    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
      <Menu.Item key="1">
        <Link to={Routes.HOME}>Products</Link>
      </Menu.Item>
      <Menu.Item key="2">
        <Link to={Routes.MANAGER_PRODUCTS}>Products Management</Link>
      </Menu.Item>
    </Menu>
  );
};

export default MenuSideBar;
