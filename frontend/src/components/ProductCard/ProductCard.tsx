import React from 'react';
import { Card, Col } from 'antd';
import { Link } from 'react-router-dom';
import { Product } from '../../store/product/types';
const { Meta } = Card;
export type ProductProps = {
  product: Product;
};
const ProductCard = ({ product }: any) => {
  return (
    <Col span={6}>
      <Link to={`/${product._id}`}>
        <Card
          hoverable
          style={{ height: 240 }}
          cover={<img alt="example" src={product.images[0]} />}
        >
          <Meta title={product.name} description={product.price} />
        </Card>
      </Link>
    </Col>
  );
};

export default ProductCard;
