import { Upload } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
export type UploadImagesProps = {
  UploadFiles(f: any): void;
};
const UploadImages = ({ UploadFiles }: UploadImagesProps) => {
  const handleChangeImage = (files: any) => {
    UploadFiles(files);
  };

  const handlePreviewImage = async (file: any) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow?.document.write(image.outerHTML);
  };
  return (
    <Upload
      name="images"
      listType="picture-card"
      beforeUpload={() => false}
      onChange={handleChangeImage}
      onPreview={handlePreviewImage}
    >
      <div>
        <PlusOutlined />
        <div style={{ marginTop: 8 }}>Upload</div>
      </div>
    </Upload>
  );
};

export default UploadImages;
