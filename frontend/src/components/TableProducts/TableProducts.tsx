import { FC } from 'react';
import { Button, Table } from 'antd';
import { Routes } from '../../types/enums';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

const TableProducts: FC = () => {
  const columns = [
    {
      title: 'STT',
      dataIndex: 'i',
      key: 'i',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Action',
      dataIndex: 'action',
      key: 'action',
      render: () => (
        <div>
          <Button type="primary" style={{ marginRight: 4 }}>
            <Link to={Routes.UPDATE_PRODUCT}>Update</Link>
          </Button>
          <Button type="primary" danger>
            Delete
          </Button>
        </div>
      ),
    },
  ];
  const products = useSelector((state: RootState) => state.product);
  const productsMap = products.products.map((x, i) => ({ ...x, i: i + 1 }));
  return (
    <>
      <h3>Table products</h3>
      <Table
        columns={columns}
        dataSource={productsMap}
        pagination={{
          position: ['bottomCenter'],
        }}
        rowKey={(row) => row._id}
      />
    </>
  );
};

export default TableProducts;
