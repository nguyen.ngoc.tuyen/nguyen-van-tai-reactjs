import React, { FC, useState } from 'react';
import { Form, Input, Select, InputNumber, Button } from 'antd';
import UploadImages from '../UploadImages';
import { formatMoney } from '../../helpers';
import { useDispatch } from 'react-redux';
import { addProduct } from '../../store/product/actions';
import { Dispatch } from 'redux';
import { uploadImages } from '../../service/productsApi';
const ProductForm: FC = () => {
  const [files, setFiles] = useState<any>([]);
  const dispatch: Dispatch<any> = useDispatch();
  const onFinish = async (values: any) => {
    const formData = new FormData();
    formData.append('images', files);
    const res = await uploadImages(formData);
    const dataFinal = { ...values, images: res.data.fileName };
    dispatch(addProduct(dataFinal));
  };
  const handleImages = (images: any) => {
    setFiles(images.file);
  };
  return (
    <>
      <h3>Add product</h3>
      <Form
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 24 }}
        layout="horizontal"
        onFinish={onFinish}
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: 'Please input your name!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Category"
          name="category"
          rules={[{ required: true, message: 'Please input your category!' }]}
        >
          <Select>
            <Select.Option value="demo">Demo</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="Type"
          name="type"
          rules={[{ required: true, message: 'Please input your type!' }]}
        >
          <Select>
            <Select.Option value="demo">Demo</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="Price"
          name="price"
          rules={[{ required: true, message: 'Please input your price!' }]}
        >
          <InputNumber formatter={formatMoney} />
        </Form.Item>
        <Form.Item label="Description" name="description">
          <Input.TextArea />
        </Form.Item>
        <Form.Item name="images" label="Thumnail" valuePropName="fileList">
          <UploadImages UploadFiles={handleImages} />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            xs: { span: 24, offset: 0 },
            sm: { span: 16, offset: 8 },
          }}
        >
          <Button type="primary" htmlType="submit">
            Add
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};
export default ProductForm;
