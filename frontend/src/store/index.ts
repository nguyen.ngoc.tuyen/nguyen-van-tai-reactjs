import { combineReducers } from 'redux';
import reducerProduct from './product/reducer';
export type RootState = ReturnType<typeof rootReducer>;

const rootReducer = combineReducers({
  product: reducerProduct,
});
export default rootReducer;
