export interface Product {
  _id: string;
  name: string;
  price: string;
  type: string;
  category: string;
  description: string;
  images: string[];
}

export interface ProductState {
  products: Product[];
  product: Product;
  totalPages: number;
  category: [];
}

export const ADD_PRODUCT = 'ADD_PRODUCT';
export const GET_PRODUCT = 'GET_PRODUCT';
export const GET_PRODUCTS = 'GET_PRODUCTS';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const UPDATE_PRODUCT = 'UPDATE_PRODUCT';
export const GET_PRODUCTS_BY_CATEGORY = 'GET_PRODUCTS_BY_CATEGORY';
export const GET_PRODUCTS_BY_TYPE = 'GET_PRODUCTS_BY_TYPE';
export const GET_PRODUCTS_BY_NAME = 'GET_PRODUCTS_BY_NAME';
export const GET_CATEGORY = 'GET_CATEGORY';
export const GET_TYPE = 'GET_TYPE';

interface AddProductAction {
  type: typeof ADD_PRODUCT;
  product: Product;
}
interface GetProductAction {
  type: typeof GET_PRODUCT;
  product: Product;
}
interface GetProductsByCategoryAction {
  type: typeof GET_PRODUCTS_BY_CATEGORY;
  products: any;
}
interface GetProductsByTypeAction {
  type: typeof GET_PRODUCTS_BY_TYPE;
  products: any;
}
interface GetProductsByNameAction {
  type: typeof GET_PRODUCTS_BY_NAME;
  products: any;
}
interface GetProductsAction {
  type: typeof GET_PRODUCTS;
  products: any;
}
interface GetCategoryAction {
  type: typeof GET_CATEGORY;
  category: any;
}
interface RemoveProductAction {
  type: typeof REMOVE_PRODUCT;
  product: Product;
}
interface UpdateProductAction {
  type: typeof UPDATE_PRODUCT;
  product: Product;
}
export type ProductActionTypes =
  | AddProductAction
  | GetProductAction
  | GetProductsAction
  | RemoveProductAction
  | UpdateProductAction
  | GetProductsByCategoryAction
  | GetProductsByTypeAction
  | GetProductsByNameAction
  | GetCategoryAction;
