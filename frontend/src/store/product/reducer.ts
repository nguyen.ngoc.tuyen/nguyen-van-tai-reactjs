import {
  ProductState,
  ProductActionTypes,
  ADD_PRODUCT,
  GET_PRODUCTS,
  GET_PRODUCT,
  REMOVE_PRODUCT,
  UPDATE_PRODUCT,
  GET_PRODUCTS_BY_CATEGORY,
  GET_PRODUCTS_BY_TYPE,
  GET_CATEGORY,
  GET_PRODUCTS_BY_NAME,
} from './types';

const initialState: ProductState = {
  products: [],
  totalPages: 1,
  product: {
    _id: '',
    name: '',
    price: '',
    type: '',
    category: '',
    description: '',
    images: [],
  },
  category: [],
};

const reducerProduct = (
  state: ProductState = initialState,
  action: ProductActionTypes
): ProductState => {
  switch (action.type) {
    case ADD_PRODUCT:
      return {
        ...state,
        products: [...state.products, action.product],
      };
    case GET_PRODUCT:
      return {
        ...state,
        product: action.product,
      };
    case GET_PRODUCTS:
      return {
        ...state,
        products: action.products.products,
        totalPages: action.products.countProducts,
      };
    case GET_CATEGORY:
      return {
        ...state,
        category: action.category,
      };
    case GET_PRODUCTS_BY_CATEGORY:
      return {
        ...state,
        products: action.products.products,
        totalPages: action.products.countProducts,
      };
    case GET_PRODUCTS_BY_NAME:
      return {
        ...state,
        products: action.products.products,
        totalPages: action.products.countProducts,
      };
    case GET_PRODUCTS_BY_TYPE:
      return {
        ...state,
        products: action.products.products,
        totalPages: action.products.countProducts,
      };
    case REMOVE_PRODUCT:
      const updatedProducts = state.products.filter(
        (product) => product._id !== action.product._id
      );
      return {
        ...state,
        products: updatedProducts,
      };
    case UPDATE_PRODUCT:
      return {
        ...state,
        products: state.products.map((product) =>
          product._id === action.product._id ? action.product : product
        ),
      };
  }
  return state;
};

export default reducerProduct;
