import {
  Product,
  ADD_PRODUCT,
  GET_PRODUCTS,
  REMOVE_PRODUCT,
  ProductActionTypes,
  GET_PRODUCT,
  GET_PRODUCTS_BY_CATEGORY,
  GET_PRODUCTS_BY_TYPE,
  GET_CATEGORY,
  GET_PRODUCTS_BY_NAME,
} from './types';
import {
  createProduct,
  getAllProduct,
  getProductById,
  getProductByCategory,
  getProductByType,
  getAllCategory,
  getProductByName,
} from '../../service/productsApi';
import { Dispatch, AnyAction } from 'redux';

export const addProduct = (product: Product) => async (
  dispatch: Dispatch<AnyAction>
) => {
  const res = await createProduct(product);
  dispatch(actAddProduct(res.data));
};

export const getProducts = (pagination: any) => async (
  dispatch: Dispatch<AnyAction>
) => {
  const res = await getAllProduct(pagination);
  dispatch(actGetProducts(res.data));
};
export const getCategory = () => async (dispatch: Dispatch<AnyAction>) => {
  const res = await getAllCategory();
  dispatch(actGetCategory(res.data));
};

export const getProduct = (id: any) => async (
  dispatch: Dispatch<AnyAction>
) => {
  const res = await getProductById(id);
  dispatch(actGetProduct(res.data));
};

export const getProductsByCategory = (category: any) => async (
  dispatch: Dispatch<AnyAction>
) => {
  const res = await getProductByCategory(category);
  dispatch(actGetProductByCategory(res.data));
};

export const getProductsByType = (category: any, type: any) => async (
  dispatch: Dispatch<AnyAction>
) => {
  const res = await getProductByType(category, type);
  dispatch(actGetProductByType(res.data));
};
export const getProductsByName = (name: any) => async (
  dispatch: Dispatch<AnyAction>
) => {
  const res = await getProductByName(name);
  dispatch(actGetProductByName(res.data));
};
export function actGetProductByName(products: Product[]): ProductActionTypes {
  return {
    type: GET_PRODUCTS_BY_NAME,
    products,
  };
}
export function actGetProductByCategory(
  products: Product[]
): ProductActionTypes {
  return {
    type: GET_PRODUCTS_BY_CATEGORY,
    products,
  };
}
export function actGetProductByType(products: Product[]): ProductActionTypes {
  return {
    type: GET_PRODUCTS_BY_TYPE,
    products,
  };
}
export function actGetCategory(category: any): ProductActionTypes {
  return {
    type: GET_CATEGORY,
    category,
  };
}

export function actGetProduct(product: Product): ProductActionTypes {
  return {
    type: GET_PRODUCT,
    product,
  };
}

export function actDeleteProduct(product: Product): ProductActionTypes {
  return {
    type: REMOVE_PRODUCT,
    product,
  };
}
export function actAddProduct(product: Product): ProductActionTypes {
  return {
    type: ADD_PRODUCT,
    product,
  };
}
export function actGetProducts(products: Product[]): ProductActionTypes {
  return {
    type: GET_PRODUCTS,
    products,
  };
}
